#!/bin/bash




for WORD in $(curl -s https://jsonplaceholder.typicode.com/todos | jq '.[0,1].title')
do
	echo $WORD | tr -d '"'
done
