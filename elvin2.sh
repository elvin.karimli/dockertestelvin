#!/bin/bash

IFS='
'


results=()

for WORD in $(curl -s https://jsonplaceholder.typicode.com/todos | jq '.[0,1].title')
do
	results+=$(echo $WORD | tr -d '"'| tr ' ' '\n')
done


results=($(sort <<<"${results[*]}"))
unset IFS
printf '%s\n' "${results[@]}"
